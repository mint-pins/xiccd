Source: xiccd
Section: x11
Priority: optional
Maintainer: Faidon Liambotis <paravoid@debian.org>
Build-Depends: debhelper (>= 10),
 libxrandr-dev,
 libglib2.0-dev,
 libcolord-dev
Standards-Version: 4.0.0
Homepage: https://github.com/agalakhov/xiccd
Vcs-Git: https://anonscm.debian.org/git/collab-maint/xiccd.git
Vcs-Browser: https://anonscm.debian.org/cgit/collab-maint/xiccd.git

Package: xiccd
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}, colord
Description: X color management daemon
 xiccd is a simple bridge between colord and X. It performs the following
 tasks:
 .
  * Enumerating displays and registering them in colord;
  * Creating default ICC profiles based on EDID data;
  * Applying ICC profiles provided by colord;
  * Maintaining the user's private ICC storage directory.
 .
 It does not depend on any particular desktop environment nor toolkit and it
 should not be used in desktop environments that support color management
 natively, like GNOME and KDE do.
